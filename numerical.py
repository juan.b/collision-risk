# -*- coding: utf-8 -*-

from mubody.mission import Mission
import numpy as np
import matplotlib.pyplot as plt
import math
from tqdm.auto import tqdm


def sphToCart(rthetaphi):
    #takes list rthetaphi (single coord)
    r       = rthetaphi[0]
    theta   = np.radians(rthetaphi[1]) # to radian
    phi     = np.radians(rthetaphi[2]) # to radian
    x = r * np.sin( theta ) * np.cos( phi )
    y = r * np.sin( theta ) * np.sin( phi )
    z = r * np.cos( theta )
    return np.array([x,y,z])

def cartToSph(xyz):
    #takes list xyz (single coord)
    x       = xyz[0]
    y       = xyz[1]
    z       = xyz[2]
    r       =  np.sqrt(x*x + y*y + z*z)
    theta   =  np.degrees(np.acos(z/r)) #to degrees
    phi     =  np.degrees(np.atan2(y,x)) #to degrees
    return np.array([r,theta,phi])


def rotation_matrix_calculation_xyzToUvw(rthetaphi):
    # takes list rthetaphi (single coord)
    # r       = rthetaphi[0]
    theta   = np.radians(rthetaphi[1]) # to radian
    phi     = np.radians(rthetaphi[2]) # to radian
    rotation_matrix_xyzToUvw = np.array([
            [ np.sin( theta ) * np.cos( phi ),  np.sin( theta ) * np.sin( phi ),   np.cos( theta )],
            [ np.cos( theta ) * np.cos( phi ),  np.cos( theta ) * np.sin( phi ),  -np.sin( theta )],
            [                  -np.sin( phi ),                    np.cos( phi ),               0.0]])
    return rotation_matrix_xyzToUvw


def print_ellipsoid(fig_number,
                    deputy_relative_vel_xyz,
                    deputy_relative_position_xyz,
                    theta_deg,
                    phi_deg):
  
    fig = plt.figure(fig_number,figsize=(10,7.5))
    ax = fig.add_subplot(111, projection='3d')
    X = deputy_relative_vel_xyz[:][0]
    Y = deputy_relative_vel_xyz[:][1]
    Z = deputy_relative_vel_xyz[:][2]

    ax.scatter(X,Y,Z, c='b', marker='o')

    max_range = np.array([X.max()-X.min(), Y.max()-Y.min(), Z.max()-Z.min()]).max() / 2.0

    mid_x = (X.max()+X.min()) * 0.5
    mid_y = (Y.max()+Y.min()) * 0.5
    mid_z = (Z.max()+Z.min()) * 0.5
    ax.set_xlim(mid_x - max_range, mid_x + max_range)
    ax.set_ylim(mid_y - max_range, mid_y + max_range)
    ax.set_zlim(mid_z - max_range, mid_z + max_range)

    ax.set_title("Relative speed ellipsoid for theta "+str(theta_deg)+" and phi "+str(phi_deg))
    ax.set_xlabel("X [m]")
    ax.set_ylabel("Y [m]")
    ax.set_zlabel("Z [m]")

    x = [0.0, deputy_relative_position_xyz[0]]
    y = [0.0, deputy_relative_position_xyz[1]]
    z = [0.0, deputy_relative_position_xyz[2]]
    ax.scatter(x, y, z, c='red', s=100)
    ax.plot(x, y, z, color='black')

    return


def propag_deputy_and_analysis_col(deputy_rel_pos_xyz,
                                   deputy_rel_vel_xyz,
                                   aTSimulation_s,
                                   t_chief_orbit_position_s,
                                   chief,
                                   deputy,
                                   dCol_m,
                                   N,
                                   comt,
                                   model,
                                   frame):   
    
    # Collision state for each deputy velocity (distance between deputy and chief is less or equal to dCol_m)
    # aCollisionN = np.zeros(N)
    
    # Collision probability by time
    aCollisionT = np.zeros(( 2, len(aTSimulation_s)) )

    IC_chief = chief.sat.s(t_chief_orbit_position_s)    
    IC_frame = chief.frame

    if comt: pbar = tqdm(total=N)
    # print(deputy_rel_vel_xyz[2][:].min())
    # plt.figure()
    for i in range(0, N, 1):

        if comt: pbar.update(1)

        #Vector de estado inicial de deputy (vector de estado del chief + offset)
        IC_deputy = IC_chief + np.array([   deputy_rel_pos_xyz[0],    deputy_rel_pos_xyz[1],    deputy_rel_pos_xyz[2],
                                         deputy_rel_vel_xyz[0][i], deputy_rel_vel_xyz[1][i], deputy_rel_vel_xyz[2][i]  ])
        IC_deputy = IC_deputy.reshape(-1,1)

       #Propagación del deputy para el tiempo mission_time
        deputy.IC(IC=IC_deputy, IC_frame=IC_frame, bar=False, frame=frame, model=model)
    
        #Calculo de la distancia en cada instante de tiempo (time) entre chief y deputy
        temp = np.zeros(len(aTSimulation_s))
        tempPSpec = np.zeros(len(aTSimulation_s))

        separation_array = np.linalg.norm(chief.sat.r(t_chief_orbit_position_s + aTSimulation_s) - deputy.sat.r(aTSimulation_s), axis=0)
        boolean_array = separation_array <= dCol_m

        try:
            idx = np.where(boolean_array==1)[0][0]
            tempPSpec = boolean_array.copy()
            temp = boolean_array.copy()
            temp[idx:] = 1
            
        except:
            tempPSpec = boolean_array.copy()
            temp = boolean_array.copy()

        aCollisionT[0] = aCollisionT[0] + temp
        aCollisionT[1] = aCollisionT[1] + tempPSpec
    
    aCollisionT[0] = aCollisionT[0][:]/N
    aCollisionT[1] = aCollisionT[1][:]/N

    if comt: pbar.close()
    
    return aCollisionT
    

def pc_profile(t0, tf, d=150.0, D=50.0, 
               q=0.2, sigma_vv=0.001, 
               N=1000, dT=20, ptype='TP',
               depRelVel=np.array([0.0, 0.0, 0.0]),
               theta=0.0, phi=0.0,
               chief=None,
               deputy=None,
               t0OrbChief=7510000.0,
               comt=False, 
               model='CRTBP',
               frame='P1-IdealSynodic',
               **kwargs):
    """
    Generates a time profile of the probability of collision

    The result is the probability that a collision has ocurred after elapsed
    time.

    Parameters
    ----------
    t0 : float
        Start time (h)
    tf : float
        End time (h)
    d : float
        Distance between satellites (m)
    D : float 
        Safety sphere radius (m)
    q : float
        Relation between standard deviations of u and v components 
        (q = sigma_v/sigma_u)
    sigma_vv : float
        Standard deviation of the v component of the velocity
    N : int
        Number of relative velocity samples for multivariate normal method
    dT : int
        Time interval to analyse distance between chief and deputy (s)
    ptype : str
        Probability type.
        TP--> risk after given time
        IP--> instantaneus risk
    depRelVel : float (np.array[3])
        Mean of relative velocity between chief and deputy
    theta : float
        Theta angle of relative position for deputy (deg)
    phi : float
        Phi angle of relative position for deputy (deg)
    t0OrbChief : float
        Time to locate initial position of chief in its orbit (s)
    comt : bool
        To activate comments

    Returns
    -------
    time : float (np.array)
        Time of simulation analysis (s)
    pc : float (np.array)
        Probability of a collision [0] (always) and
        instantaneous prob of collision [1] (if ptype='IP')
    """

    t_chief_orbit_position_s = t0OrbChief

    tSimulation_s = tf-t0
    # tSimulation_s = tSimulation_h * (3600.0) #to be defined

    k = 1.0/q
    sigma_uv = sigma_vv/q 

    d_m = d
    theta_deg = theta
    phi_deg = phi

    dCol_m = D #distance to consider a collision

    mean = depRelVel

    #Intervalo de tiempo en el que se van a analizar las diferencias entre trayectorias de chief y deputy [s]
    dt_s = dT
    #Número de intervalos según la duración de cada intervalo (dt_s) y el tiempo total a analizar
    nInterTSimul = math.ceil(tSimulation_s/dt_s)
    #Array de tiempo para analizar durante la propagación del deputy
    aTSimulation_s = np.linspace(0, tSimulation_s, nInterTSimul + 1)

    if comt: print(" phi_deg:"+str(phi_deg))
    if comt: print("  theta_deg:"+str(theta_deg))        
    if comt: print("   d:"+str(d_m))
    
    deputy_relative_position_sph = np.array([d_m, theta_deg, phi_deg])
                  
    rotation_matrix_xyzToUvw = rotation_matrix_calculation_xyzToUvw(deputy_relative_position_sph)
    rotation_matrix_uvwToXyz = np.transpose(rotation_matrix_xyzToUvw)
                
    #deputy_relative_position_uvw = np.array([u, v, w])
                
    deputy_relative_position_xyz = sphToCart(deputy_relative_position_sph)
    #deputy_position_xyz = chief_cond[0:3] + deputy_relative_position_xyz
    
    if comt: print("    sigma_u:"+str(sigma_uv))
    if comt: print("     N:"+str(N))    
    if comt: print("      k:"+str(k))
    
    cov_matrix_uvw = np.array([[pow(sigma_uv,2),               0.0,               0.0],
                               [            0.0, pow(sigma_uv/k,2),               0.0],
                               [            0.0,               0.0, pow(sigma_uv/k,2)]])
                            
    #Covariance matrix for xyz frame (C' = R C RT)
    cov_matrix_xyz = np.dot(np.dot(rotation_matrix_uvwToXyz, 
                                   cov_matrix_uvw),
                            rotation_matrix_xyzToUvw)
                        
                            
    deputy_relative_vel_xyz = np.random.multivariate_normal(mean, cov_matrix_xyz, N).T
                            
    aPCollisionT = propag_deputy_and_analysis_col(deputy_relative_position_xyz,
                                                  deputy_relative_vel_xyz,
                                                  aTSimulation_s,
                                                  t_chief_orbit_position_s,
                                                  chief,
                                                  deputy,
                                                  dCol_m,
                                                  N,
                                                  comt,
                                                  model,
                                                  frame)
    
    return aTSimulation_s, aPCollisionT
