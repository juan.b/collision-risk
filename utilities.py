import os
import pickle
import numpy as np


def store(data, file_name, dir_name):
    """
    Stores data in pickle object

    Parameters
    ----------
    data : - 
        Data to be stored
    file_name : str
        File name
    dir_name : str
        Folder name
    """

    file_path = "Results/" + dir_name 

    os.makedirs(file_path, exist_ok=True)

    file_handler = open(file_path + "/" + file_name, 'wb')

    pickle.dump(data, file_handler)

    file_handler.close()

    return 0

def recover(file_name, dir_name):
    """
    Recovers data from pickle object

    Parameters
    ----------
    file_name : str
        File name
    dir_name : str
        Folder name

    Returns
    -------
    data : -
        Data retrieved
    """

    file_path = "Results/" + dir_name 
    file_handler = open(file_path + "/" + file_name, 'rb')

    data = pickle.load(file_handler)

    file_handler.close()

    return data

def rmse(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())

