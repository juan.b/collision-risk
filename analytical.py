from tkinter import N
import numpy as np


def solve_eq(a, b, c, ft):
    """
    Solves second degree polynomic equation

    Parameters
    ----------
    a : float
        Coefficient of the second degree term
    b : float
        Coefficient of the first degree term
    c : float
        Constant term
    ft : int
        Indicates the sign of the solution

    Returns
    -------
    x : float
        Solution
    """

    x = (-b + ft * np.sqrt(b**2 - 4 * a * c))/(2 * a)

    return x


def collision_time(d, D, n, sigma_vu):
    """
    Returns time of contact between error ellipsoid and safety sphere

    Parameters
    ----------
    d : float
        Distance between satellites
    D : float
        Safety sphere radius
    n : int
        Size of the error ellipsoid. n-sigma parameter
    sigma_vu : float
        Standard deviation of the u component of the velocity

    Returns
    -------
    t : float
        Solution
    """

    t = (d - D)/(n * sigma_vu)

    return t

def region_times(d, D, n, q, sigma_vv):
    """
    Computes time for which the intersection between error ellipsoid and
    safety sphere changes

    Parameters
    ----------
    d : float
        Distance between satellites
    D : float
        Safety sphere radius
    n : int
        Size of the error ellipsoid. n-sigma parameter
    q : float
        Relation between standard deviations of u and v components
        (q = sigma_v/sigma_u)
    sigma_vu : float
        Standard deviation of the u component of the velocity

    Returns
    -------
    t1 : float
        Time for which the safety sphere and the error ellipsoid overalap
    t2 : float
        Time for which the error ellipsoid outgrows safety sphere in u direcction
    tstar : float
        Time for which the safety sphere is completely absorbed by the error
        ellipsoid
    """

    sigma_vu = sigma_vv/q

    t1 = (d - D)/(n * sigma_vu)

    t2 = (d + D)/(n * sigma_vu)

    Re = q * sigma_vv*t2 * n
    # print(Re, D)
    if Re>D:
        tstar = t2
    else:
        tstar = d/(n*sigma_vv) * np.sqrt(q**2/(1 - q**2) + D**2/d**2)

    return t1, t2, tstar


def u_star_sphere(d, D, rho, q, sigma_v, ft):
    """
    Returns u coordinate of the instersection between ellipsoid and
    circumference

    Parameters
    ----------
    d : float
        Distance between satellites
    D : float
        Safety sphere radius
    rho : float
        Size of the ellipsoid
    q : float
        Relation between standard deviations of u and v components
        (q = sigma_v/sigma_u)
    sigma_v : float
        Standard deviation of the v component of the position
    ft : int
        Indicates the sign of the solution

    Returns
    -------
    u : float
        Solution

    """

    a = q**2 - 1
    b = 2 * d
    c = -d**2 + D**2 - rho**2 * sigma_v**2

    if np.isclose(q, 1):
        u = -c/b
    elif np.isclose((b**2 - 4 * a * c), 0):
        u = -b/(2 * a)
    else:
        u = solve_eq(a, b, c, ft)

    return u


def u_star_tangent(d, D, rho, q, sigma_v):
    """
    Returns u coordinate of the instersection between ellipsoid and line tangent
    to the safety sphere

    Parameters
    ----------
    d : float
        Distance between satellites
    D : float
        Safety sphere radius
    rho : float
        Size of the ellipsoid
    q : float
        Relation between standard deviations of u and v components
        (q = sigma_v/sigma_u)
    sigma_v : float
        Standard deviation of the v component of the position

    Returns
    -------
    u : float
        Solution
    """

    alpha = np.arcsin(D/d)

    u = np.sqrt(rho**2 * sigma_v**2/(q**2 + np.tan(alpha)**2))

    return u


def u_star(d, D, rho, q, sigma_v, ft=1, ptype='TP'):
    """
    Returns u coordinate of the instersection between error ellipsoid and
    collision volume

    Parameters
    ----------
    d : float
        Distance between satellites
    D : float
        Safety sphere radius
    rho : float
        Size of the ellipsoid
    q : float
        Relation between standard deviations of u and v components
        (q = sigma_v/sigma_u)
    sigma_v : float
        Standard deviation of the v component of the position
    ft : int
        Indicates the sign of the solution
    ptype : str
        Probability type. It defines the collision volume
        TP--> risk after given time
        IP--> instantaneus risk

    Returns
    -------
    u : float
        Solution
    """

    if (rho > rho_sphere2tangent(d, D, q, sigma_v)) and (ptype=='TP'):
        u = u_star_tangent(d, D, rho, q, sigma_v)
    # elif (rho > rho_middle(d, D, q, sigma_v)) and (ptype=='IP'):
    #     u = u_star_sphere(d, D, rho, q, sigma_v, -1)
    else:
        u = u_star_sphere(d, D, rho, q, sigma_v, ft)

    return u


def rho_middle(d, D, q, sigma_v):
    """
    Returns the size of the ellipsoid (rho) for which the u_star solution
    changes sign when Re>D

    Parameters
    ----------
    d : float
        Distance between satellites
    D : float
        Safety sphere radius
    q : float
        Relation between standard deviations of u and v components
        (q = sigma_v/sigma_u)
    sigma_v : float
        Standard deviation of the v component of the position

    Returns
    -------
    rho : float
        Size of the error ellipsoid
    """

    sigma_u = sigma_v / q

    rho = np.sqrt(d**2/sigma_u**2 + D**2/sigma_v**2)
    # print(d/sigma_u, D/sigma_v, np.sqrt(rho))

    return rho

def eq_circle_v(d, D, u):

    v = np.sqrt(D**2 - (u - d)**2)
    return v

def eq_ellipsoid_rho(u, v, sigma_u, sigma_v):

    rho = np.sqrt(u**2/sigma_u**2 + v**2/sigma_v**2)

    return rho


def rho_sphere2tangent(d, D, q, sigma_v):
    """
    Returns the size of the ellipsoid (rho) for which the collision area changes
    from sphere to tangent

    Parameters
    ----------
    d : float
        Distance between satellites
    D : float
        Safety sphere radius
    q : float
        Relation between standard deviations of u and v components
        (q = sigma_v/sigma_u)
    sigma_v : float
        Standard deviation of the v component of the position

    Returns
    -------
    rho : float
        Size of the error ellipsoid
    """

    alpha = np.arcsin(D/d)

    u = d - D * np.sin(alpha)
    v = D * np.cos(alpha)
    sigma_u = sigma_v / q

    rho = np.sqrt(u**2/sigma_u**2 + v**2/sigma_v**2)

    return rho


def diff_volume(rho, q, sigma_v, h):
    """
    Returns the differential volume of the spheroid cap resulting from the
    intersection between the error ellipsoid and the collision volume

    Parameters
    ----------
    rho : float
        Size of the ellipsoid
    q : float
        Relation between standard deviations of u and v components
        (q = sigma_v/sigma_u)
    sigma_v : float
        Standard deviation of the v component of the position
    h : float
        Cap height

    Returns
    -------
    dV : float
        Differential volume
    """

    sigma_u = sigma_v/q

    dV = 2 * np.pi * q**2 * rho * sigma_u**2 * (h)

    return dV


def pdf(rho):
    """
    Probability density function
    """

    pdf = np.exp(-rho**2/2)

    return pdf


def pc_integrand_cap(d, D, rho, q, sigma_v, ptype):
    """
    Returns the integrand of the collision risk integral for a cap volume
    as a function of rho

    Parameters
    ----------
    d : float
        Distance between satellites
    D : float
        Safety sphere radius
    rho : float
        Size of the ellipsoid
    q : float
        Relation between standard deviations of u and v components
        (q = sigma_v/sigma_u)
    sigma_v : float
        Standard deviation of the v component of the position
    ptype : str
        Probability type. It defines the collision volume
        TP--> risk after given time
        IP--> instantaneus risk

    Returns
    -------
    pc_integrand : float
        Integrand
    """

    sigma_u = sigma_v/q

    ustar = u_star(d, D, rho, q, sigma_v, ft=1, ptype=ptype)

    h = rho * sigma_u - ustar

    dV = diff_volume(rho, q, sigma_v, h)

    pc_integrand = (1/(np.sqrt(8*np.pi**3) * sigma_u * sigma_v**2)) * pdf(rho) * dV

    return pc_integrand


def pc_integrand_section(d, D, rho, q, sigma_v, ptype):
    """
    Returns the integrand of the collision risk integral for a section of the
    ellipsoid as a function of rho

    Parameters
    ----------
    d : float
        Distance between satellites
    D : float
        Safety sphere radius
    rho : float
        Size of the ellipsoid
    q : float
        Relation between standard deviations of u and v components
        (q = sigma_v/sigma_u)
    sigma_v : float
        Standard deviation of the v component of the position
    ptype : str
        Probability type. It defines the collision volume
        TP--> risk after given time
        IP--> instantaneus risk

    Returns
    -------
    pc_integrand : float
        Integrand
    """

    sigma_u = sigma_v/q

    ustar1 = u_star(d, D, rho, q, sigma_v, ft=1, ptype=ptype)
    ustar2 = u_star(d, D, rho, q, sigma_v, ft=-1, ptype=ptype)

    h1 = rho * sigma_u - ustar1
    h2 = rho * sigma_u - ustar2

    dV = diff_volume(rho, q, sigma_v, h1) - diff_volume(rho, q, sigma_v, h2)

    pc_integrand = (1/(np.sqrt(8*np.pi**3) * sigma_u * sigma_v**2)) * pdf(rho) * dV

    return pc_integrand


def pc_t(d, D, n, q, sigma_vv, t, Na, ptype):
    """
    Returns the probability of a collision after a given time.

    The probability is computed integrating the probability density function in
    the intersected volume between the error ellipsoid and the collision region.
    The integration is done numerically using the trapezoid method.

    Parameters
    ----------
    d : float
        Distance between satellites
    D : float
        Safety sphere radius
    n : float
        Size of the error ellipsoid
    q : float
        Relation between standard deviations of u and v components
        (q = sigma_v/sigma_u)
    sigma_vv : float
        Standard deviation of the v component of the velocity
    t : float
        Time of interest
    Na : int
        Number of points for the discretization of the integral
    ptype : str
        Probability type. It defines the collision volume
        TP--> risk after given time
        IP--> instantaneus risk

    Returns
    -------
    pc : float
        Probability of a collision
    """

    sigma_vu = sigma_vv/q

    t1, t2, t3 = region_times(d, D, n, q, sigma_vv)
    #t1->ellipsoid and sphere encounters, collision risk is not zero anymore

    Re = q * sigma_vv*t2 * n

    #
    if t <= t1:
        #before contact, risk is zero
        pc_t = 0
    else:
        #size of ellipoid
        sigma_u = sigma_vu * t
        sigma_v = sigma_vv * t

        if ptype is 'TP':
            #the integration region is from contact to the boundary of the
            #ellipsoid (n), as it includes the safety sphere and its back region
            rho_1 = (d - D)/sigma_u
            rho_2 = n

            rho_array = np.linspace(rho_1, rho_2, Na)
            pc_t_array = np.zeros_like(rho_array)

            for idx, rho in enumerate(rho_array):
                pc_t_array[idx] = pc_integrand_cap(d, D, rho, q, sigma_v, ptype)
        else:

            if t < t2:
                #the ellipsoid has not reached the u limit of the safety sphere
                rho_1 = (d - D)/sigma_u
                rho_2 = n

                rho_array = np.linspace(rho_1, rho_2, Na)
                pc_t_array = np.zeros_like(rho_array)

                for idx, rho in enumerate(rho_array):
                    pc_t_array[idx] = pc_integrand_cap(d, D, rho, q, sigma_v, ptype)
            else:
                #the ellipsoid has reached the u limit of the safety sphere. It
                #may or may not have absorbed the sphere completely
                if Re>D:
                    #sphere is completely absorbed, the integration is between
                    #the contact point and the exit point
                    rho_1 = (d - D)/sigma_u
                    rho_2 = (d + D)/sigma_u
                    # print(rho_1*sigma_u,rho_2*sigma_u,u_star_sphere(d, D, rho_1, q, sigma_v, 1),u_star_sphere(d, D, rho_2, q, sigma_v, -1))
                    # print(rho_1, rho_2, d/sigma_u, D/sigma_v, np.sqrt((d/sigma_u)**2 + (D/sigma_v)**2), sigma_u, sigma_v)

                    rho_array = np.linspace(rho_1, rho_2, Na)
                    pc_t_array = np.zeros_like(rho_array)


                    for idx, rho in enumerate(rho_array):
                        pc_t_array[idx] = pc_integrand_cap(d, D, rho, q, sigma_v, ptype)
                else:
                    if t<t3:
                        rho_1 = (d - D)/sigma_u
                        rho_2 = (d + D)/sigma_u
                        rho_3 = n

                        rho_array = np.linspace(rho_1, rho_3, Na)
                        pc_t_array = np.zeros_like(rho_array)

                        for idx, rho in enumerate(rho_array):
                            if rho < rho_2:
                                pc_t_array[idx] = pc_integrand_cap(d, D, rho, q, sigma_v, ptype)
                            else:
                                pc_t_array[idx] = pc_integrand_section(d, D, rho, q, sigma_v, ptype)
                    else:
                        rho_1 = (d - D)/sigma_u
                        rho_2 = (d + D)/sigma_u

                        u_t3 = u_star_sphere(d, D, n, q, sigma_vv*t3, -1)
                        v_t3 = eq_circle_v(d, D, u_t3)
                        rho_t3 = eq_ellipsoid_rho(u_t3, v_t3, sigma_vu*t, sigma_vv * t)
                        rho_3 = rho_t3

                        rho_array = np.linspace(rho_1, rho_3, Na)
                        pc_t_array = np.zeros_like(rho_array)



                        for idx, rho in enumerate(rho_array):
                            if rho < rho_2:
                                pc_t_array[idx] = pc_integrand_cap(d, D, rho, q, sigma_v, ptype)
                            else:
                                pc_t_array[idx] = pc_integrand_section(d, D, rho, q, sigma_v, ptype)

                        if np.any(np.isnan(pc_t_array)):


                            a = q**2 - 1
                            b = 2 * d
                            c = -d**2 + D**2 - rho**2 * sigma_v**2
                            res = b**2 - 4 * a * c
                            f = 3
                            pass

        pc_t = np.trapz(pc_t_array, x=rho_array)

    return pc_t


def pc_profile(t0, tf, d, D, n, q, sigma_vv, Na=100, dT=5, ptype='TP', **kwargs):
    """
    Generates a time profile of the probabilit of collision

    The result is the probability that a collision has ocurred after elapsed
    time.

    Parameters
    ----------
    t0 : float
        Start time
    tf : float
        End time
    d : float
        Distance between satellites
    D : float
        Safety sphere radius
    n : float
        Size of the error ellipsoid
    q : float
        Relation between standard deviations of u and v components
        (q = sigma_v/sigma_u)
    sigma_vv : float
        Standard deviation of the v component of the velocity
    Na : int
        Number of points for the discretization of the integral
    ptype : str
        Probability type.
        TP--> risk after given time
        IP--> instantaneus risk

    Returns
    -------
    pc : float
        Probability of a collision
    """

    time = np.arange(np.floor(t0), np.floor(tf) + 1, step=dT)

    p = np.zeros_like(time)

    for i in range(len(time)):
        p[i] = pc_t(d, D, n, q, sigma_vv, time[i], Na, ptype)

    return time, p

